const port = process.env.PORT || 6666
const server = require('./src/server/api');

process.on('uncaughtException', ()=> {
  logger.error(err);
  process.exit(1);
});

server.listen(port, (err) => {
  if (err) throw err;
  console.log(`Server listening on http://localhost:${port}`)
});
